import itertools
from pathlib import Path

import matplotlib.pyplot as plt
import csv
import argparse


def getList(filename: Path, idx: int):
    with open(filename, "r") as csv_file:
        rows = csv.reader(csv_file, delimiter=';')
        res = [[], []]
        for row in rows:
            res[0].append(int(row[0]))
            res[1].append(int(row[idx]))

        return res


def createPlot(path: Path, idx: int):
    plt.figure(idx)
    name = ""
    if idx == 1:
        name = "calc_time"
    elif idx == 2:
        name = "process_time"
    elif idx == 3:
        name = "avg_time"
    else:
        print("Invalid idx")

    async_path = path / "async.csv"
    blocking_path = path / "blocking.csv"
    non_blocking_path = path / "non_blocking.csv"
    if async_path.exists():
        async_data = getList(async_path, idx)
        plt.plot(async_data[0], async_data[1], color='red', label='async', ls='dashdot')

    if blocking_path.exists():
        blocking_data = getList(blocking_path, idx)
        plt.plot(blocking_data[0], blocking_data[1], color='blue', label='blocking', ls='solid')

    if non_blocking_path.exists():
        non_blocking_data = getList(non_blocking_path, idx)
        plt.plot(non_blocking_data[0], non_blocking_data[1], color='green', label='non_blocking', ls='dashed')

    label = "_".join(itertools.takewhile(lambda x: not x.isnumeric(), path.name.split(sep='_')),)
    plt.xlabel(label)
    plt.ylabel("Time, ms")
    plt.title(name)

    plt.legend()
    plt.grid()
    plt.savefig(path /( name + ".png"))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("folder")
    args = parser.parse_args()

    path = Path(args.folder)
    for i in range(1, 4):
        createPlot(path, i)

