import com.google.protobuf.gradle.proto
import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc

apply {
    plugin("com.google.protobuf")
}

dependencies {
    implementation("com.google.protobuf:protobuf-java:3.19.6")
}

sourceSets {
    main {
        proto {
            srcDir("src/proto")
        }
        java.setSrcDirs(listOf("src/main/java"))
    }
}

protobuf {
    generatedFilesBaseDir = "$projectDir/src/"
    protoc {
        artifact = "com.google.protobuf:protoc:3.19.6"
    }
}

tasks.processResources {
    dependsOn(":common:generateProto")
}
