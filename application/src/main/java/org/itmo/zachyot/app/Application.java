package org.itmo.zachyot.app;

import org.itmo.zachyot.client.Client;
import org.itmo.zachyot.client.Metric;
import org.itmo.zachyot.client.RequestRepo;
import org.itmo.zachyot.server.Server;
import org.itmo.zachyot.server.async.AsyncServer;
import org.itmo.zachyot.server.blocking.BlockingServer;
import org.itmo.zachyot.server.non_blocking.NonBlockingServer;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Application {

    public enum DynamicParam {
        DATA_SIZE,
        CLIENT_COUNT,
        DELAY
    }

    private record Param(String name, long value) {
    }

    /// STATIC
    private static final String ASYNC_ARCH = "async";
    private static final String BLOCKING_ARCH = "blocking";
    private static final String NON_BLOCKING_ARCH = "non_blocking";
    private static final String REQUESTS_COUNT_NAME = "requests_count";
    private static final String DATA_SIZE_NAME = "data_size";
    private static final String CLIENT_COUNT_NAME = "client_count";
    private static final String DELAY_NAME = "delay";

    private static final Path OUTPUT_DIR_PATH = Path.of("out");
    private static final String DELIM = ";";

    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 8081;

    /// STATE
    private final ExecutorService clientThreadPool = Executors.newCachedThreadPool();
    private final Server server;
    private final File outputFile;
    private final int requestsCount;
    private final int[] dataSize;
    private final int[] clientCounts;
    private final int[] delays;
    private final RunStrategy runStrategy;

    public Application(@NotNull String arch,
                       int requestsCount,
                       int @NotNull [] dataSize,
                       int @NotNull [] clientCounts,
                       int @NotNull [] delays,
                       @NotNull DynamicParam dynamicParam
    ) throws IOException {

        this.server = switch (arch.toLowerCase()) {
            case BLOCKING_ARCH -> new BlockingServer(SERVER_PORT);
            case NON_BLOCKING_ARCH -> new NonBlockingServer(SERVER_PORT);
            case ASYNC_ARCH -> new AsyncServer(SERVER_PORT);
            default -> throw new IOException("Unsupported architecture: " + arch);
        };

        this.requestsCount = requestsCount;
        this.dataSize = dataSize;
        this.clientCounts = clientCounts;
        this.delays = delays;

        this.outputFile = createOutputFile(dynamicParam, arch.toLowerCase());

        this.runStrategy = switch (dynamicParam) {
            case DATA_SIZE -> new DataSizeStrategy();
            case CLIENT_COUNT -> new ClientCountStrategy();
            case DELAY -> new DelayStrategy();
        };
    }

    private @NotNull File createOutputFile(@NotNull DynamicParam dynamicParam, @NotNull String arch) throws IOException {
        final String innerDirName = switch (dynamicParam) {
            case DATA_SIZE -> getDirName(DATA_SIZE_NAME, dataSize,
                    new Param(CLIENT_COUNT_NAME, clientCounts[0]),
                    new Param(DELAY_NAME, delays[0]),
                    new Param(REQUESTS_COUNT_NAME, requestsCount));
            case CLIENT_COUNT -> getDirName(CLIENT_COUNT_NAME, clientCounts,
                    new Param(DATA_SIZE_NAME, dataSize[0]),
                    new Param(DELAY_NAME, delays[0]),
                    new Param(REQUESTS_COUNT_NAME, requestsCount));
            case DELAY -> getDirName(DELAY_NAME, delays,
                    new Param(DATA_SIZE_NAME, dataSize[0]),
                    new Param(CLIENT_COUNT_NAME, clientCounts[0]),
                    new Param(REQUESTS_COUNT_NAME, requestsCount));
        };

        final Path dirPath = OUTPUT_DIR_PATH.resolve(innerDirName);
        Files.createDirectories(dirPath);
        final Path filePath = dirPath.resolve(arch.toLowerCase() + ".csv");
        final File file = new File(filePath.toUri());
        file.createNewFile();
        return file;
    }

    private static @NotNull String getDirName(@NotNull String dynParamName,
                                              int @NotNull [] dynParamValues,
                                              @NotNull Param... params
    ) {
        final String delim = "_";
        final StringBuilder builder = new StringBuilder(dynParamName);
        for (var val : dynParamValues) {
            builder.append(delim).append(val);
        }
        for (var param : params) {
            builder.append(delim).append(param.name).append(delim).append(param.value);
        }
        return builder.toString();
    }

    private @NotNull Metric runOneIter(int size, int clientCount, @NotNull Duration delay) throws IOException {
        Metric result = null;
        try {
            // Можно было бы создавать репу с реквестом не на каждой итерации,
            // а в тестах с изменяющимися задержкой или количеством клиентов создать единожды
            // и везде использовать его, но так проще и быстрее было написать :)
            RequestRepo repo = new RequestRepo(size);
            List<Client> clients = new ArrayList<>(clientCount);
            List<Future<Metric>> futures = new ArrayList<>(clientCount);

            for (int i = 0; i < clientCount; ++i) {
                Client client = new Client(SERVER_IP, SERVER_PORT, requestsCount, repo, delay);
                clients.add(client);
            }

            clients.forEach(cl -> futures.add(clientThreadPool.submit(cl)));

            Metric metric = new Metric(0, 0, 0, 0);
            for (var f : futures) {
                metric = metric.updateTime(f.get());
            }

            for (var cl : clients) {
                cl.close();
            }

            result = new Metric(0, metric.calcTimeMillis(), metric.processTimeMillis(), Math.max(0, (metric.requestTimeMillis() - clientCount * delay.toMillis() * requestsCount) / clientCount));
        } catch (Exception exc) {
            throw new IOException(exc);
        }

        // Дадим gc очистить память перед следующей итерацией
        try {
            System.gc();
            Thread.sleep(Duration.ofSeconds(5));
        } catch (InterruptedException ignored) {
        }
        return result;
    }

    private interface RunStrategy {
        void run() throws IOException;
    }

    private class DataSizeStrategy implements RunStrategy {
        @Override
        public void run() throws IOException {
            final int clientCount = clientCounts[0];
            final Duration delay = Duration.ofMillis(delays[0]);

            try (PrintWriter writer = new PrintWriter(new FileOutputStream(outputFile))) {
                final int iterCnt = (dataSize[2] - dataSize[0] + dataSize[1]) / dataSize[1];
                for (int currSize = dataSize[0], i = 1; currSize <= dataSize[2]; currSize += dataSize[1], ++i) {
                    Metric result = new Metric(currSize, 0, 0, 0);
                    result = result.updateTime(runOneIter(currSize, clientCount, delay));
                    writer.println(result.toCsvString(DELIM));
                    System.out.println("Done iter " + i + "/" + iterCnt);
                }
            }
        }
    }

    private class ClientCountStrategy implements RunStrategy {

        @Override
        public void run() throws IOException {
            final int size = dataSize[0];
            final Duration delay = Duration.ofMillis(delays[0]);

            try (PrintWriter writer = new PrintWriter(new FileOutputStream(outputFile))) {
                final int iterCnt = (clientCounts[2] - clientCounts[0] + clientCounts[1]) / clientCounts[1];
                for (int currCount = clientCounts[0], i = 1; currCount <= clientCounts[2]; currCount += clientCounts[1], ++i) {
                    Metric result = new Metric(currCount, 0, 0, 0);
                    result = result.updateTime(runOneIter(size, currCount, delay));
                    writer.println(result.toCsvString(DELIM));
                    System.out.println("Done iter " + i + "/" + iterCnt);
                }
            }
        }
    }

    private class DelayStrategy implements RunStrategy {
        @Override
        public void run() throws IOException {
            final int size = dataSize[0];
            final int clientCount = clientCounts[0];

            try (PrintWriter writer = new PrintWriter(new FileOutputStream(outputFile))) {
                final int iterCnt = (delays[2] - delays[0] + delays[1]) / delays[1];
                for (int currDelay = delays[0], i = 1; currDelay <= delays[2]; currDelay += delays[1], ++i) {
                    Metric result = new Metric(currDelay, 0, 0, 0);
                    result = result.updateTime(runOneIter(size, clientCount, Duration.ofMillis(currDelay)));
                    writer.println(result.toCsvString(DELIM));
                    System.out.println("Done iter " + i + "/" + iterCnt);
                }
            }
        }
    }

    public void run() throws IOException {
        Thread serverThread = new Thread(server);
        try {
            serverThread.start();
            runStrategy.run();
        } finally {
            clientThreadPool.shutdownNow();
            server.close();
        }
    }

}
