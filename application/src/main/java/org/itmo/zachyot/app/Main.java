package org.itmo.zachyot.app;

import org.itmo.zachyot.app.Application.DynamicParam;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Arrays;


public class Main {

    private static final String ARCH_PARAM = "--arch=";
    private static final String REQUESTS_PARAM = "--requests=";
    private static final String DATA_SIZE_PARAM = "--data-size=";
    private static final String CLIENTS_PARAM = "--clients=";
    private static final String DELAY_PARAM = "--delay=";
    private static final String PARAM_DELIMITER = "_";

    private static int @NotNull [] getParamValue(@NotNull String param) throws IOException {
        final String[] arr = param.split(PARAM_DELIMITER);
        if (arr.length == 1) {
            return new int[]{Integer.parseInt(arr[0])};
        } else if (arr.length == 3) {
            final int[] res = Arrays.stream(arr).mapToInt(Integer::parseInt).toArray();
            if (res[0] > res[2] || res[1] < 1) throw new IOException("Invalid parameter value:" + param);
            return res;
        } else {
            throw new IOException("Invalid parameter value: " + param);
        }
    }

    public static void main(String[] args) {
        String arch = null;
        int requestsCount = 0;
        int[] dataSize = {};
        int[] clientCounts = {};
        int[] delays = {};
        DynamicParam dynamicParam = null;

        try {
            for (String arg : args) {
                if (arg.startsWith(ARCH_PARAM)) {
                    arch = arg.replaceFirst(ARCH_PARAM, "");
                } else if (arg.startsWith(REQUESTS_PARAM)) {
                    requestsCount = Integer.parseInt(arg.replaceFirst(REQUESTS_PARAM, ""));
                } else if (arg.startsWith(DATA_SIZE_PARAM)) {
                    dataSize = getParamValue(arg.replaceFirst(DATA_SIZE_PARAM, ""));
                } else if (arg.startsWith(CLIENTS_PARAM)) {
                    clientCounts = getParamValue(arg.replaceFirst(CLIENTS_PARAM, ""));
                } else if (arg.startsWith(DELAY_PARAM)) {
                    delays = getParamValue(arg.replaceFirst(DELAY_PARAM, ""));
                } else {
                    throw new IOException("Invalid argument: " + arg);
                }
            }

            if (arch == null) throw new IOException("Architecture is not specified");
            if (requestsCount < 1) throw new IOException("Invalid requests count");
            if (dataSize.length == 0) throw new IOException("Param data_size is not specified");
            if (clientCounts.length == 0) throw new IOException("Param clients is not specified");
            if (delays.length == 0) throw new IOException("Param delay is not specified");
            if (dataSize.length + clientCounts.length + delays.length != 5)
                throw new IOException("Invalid parameter values");

            if (dataSize.length == 3) {
                dynamicParam = DynamicParam.DATA_SIZE;
            } else if (clientCounts.length == 3) {
                dynamicParam = DynamicParam.CLIENT_COUNT;
            } else if (delays.length == 3) {
                dynamicParam = DynamicParam.DELAY;
            } else {
                throw new IOException("Invalid parameter values");
            }

            Application app = new Application(arch, requestsCount, dataSize, clientCounts, delays, dynamicParam);
            app.run();
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
        }

//        RequestRepo repo = new RequestRepo(10_000);
//
//        try (ExecutorService clThreadPool = Executors.newCachedThreadPool();
//             Server server = new AsyncServer(8081);
//        ) {
//            Thread thread = new Thread(server);
//            thread.start();
//
//            List<Client> clients = new ArrayList<>();
//            List<Future<Metric>> futures = new ArrayList<>();
//            for (int i = 0; i < 1000; ++i) {
//                Client client = new Client("127.0.0.1", 8081, 50, repo, Duration.ofMillis(100));
//                clients.add(client);
//            }
//            for (var cl : clients) {
//                futures.add(clThreadPool.submit(cl));
//            }
//
//            for (var f : futures) {
//                Metric m = f.get();
//                System.out.println(m.toString());
//            }
//
//            for (var cl : clients) {
//                cl.close();
//            }
//
//        } catch (Exception exc) {
//            System.out.println(exc.getMessage());
//        }
    }

}
