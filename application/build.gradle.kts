plugins {
    application
}

configure<JavaApplication> {
    mainClass.set("org.itmo.zachyot.app.Main")
}

group = "org.itmo.zachyot"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":client"))
    implementation(project(":server"))
    implementation("org.jetbrains:annotations:24.0.0")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

sourceSets {
    main {
        java.setSrcDirs(listOf("src/main/java"))
        resources.setSrcDirs(listOf("resources"))
    }
    test {
        java.setSrcDirs(listOf("src/test/java"))
        resources.setSrcDirs(listOf("testResources"))
    }
}

tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("org.itmo.zachyot.app.Main")
    applicationDefaultJvmArgs = setOf("-Xmx4g")
    executableDir = rootDir.parent
}