package org.itmo.zachyot.client;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;


public record Metric(long param, long calcTimeMillis, long processTimeMillis, long requestTimeMillis) {

    @Contract("_ -> new")
    public @NotNull Metric updateTime(@NotNull Metric from) {
        return new Metric(this.param,
                this.calcTimeMillis + from.calcTimeMillis,
                this.processTimeMillis + from.processTimeMillis,
                this.requestTimeMillis + from.requestTimeMillis);
    }

    public @NotNull String toCsvString(@NotNull String delim) {
        return this.param + delim + this.calcTimeMillis + delim + this.processTimeMillis + delim + this.requestTimeMillis;
    }

}
