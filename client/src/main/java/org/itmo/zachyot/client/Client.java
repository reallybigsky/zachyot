package org.itmo.zachyot.client;

import org.itmo.zachyot.SortResponse;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Callable;


public class Client implements Callable<Metric>, Closeable {

    private static final int READ_BUFFER_SIZE = 100 * 1024;
    private final ByteBuffer readBuffer = ByteBuffer.allocate(READ_BUFFER_SIZE);

    private final Socket socket;
    private final int totalRequests;

    // Сохраним ссыль на запрос извне, чтобы сэкономить память на клиентах,
    // так как смотрим не на их производительность
    private final RequestRepo requestRepo;
    private final Duration delay;

    public Client(@NotNull String ip,
                  int port,
                  int totalRequests,
                  @NotNull RequestRepo requestRepo,
                  @NotNull Duration delay
    ) throws IOException {
        if (totalRequests < 1)
            throw new IOException("Requests counter must be positive");

        this.socket = new Socket(ip, port);
        this.totalRequests = totalRequests;
        this.requestRepo = requestRepo;
        this.delay = delay;
    }

    @Override
    public Metric call() throws IOException, InterruptedException {
        final byte[] request = requestRepo.getRequest().toByteArray();
        long calcTimeMillis = 0;
        long processTimeMillis = 0;
        final Instant start = Instant.now();

        for (int i = 0; i < totalRequests; ++i) {
            new DataOutputStream(socket.getOutputStream()).writeInt(request.length);
            socket.getOutputStream().write(request);
            socket.getOutputStream().flush();

            SortResponse response = readResponse();
            if (!requestRepo.checkSorted(response.getDataList()))
                throw new IOException("Data is not sorted");

            calcTimeMillis += response.getCalcTimeMillis();
            processTimeMillis += response.getProcessTimeMillis();

            Thread.sleep(delay);
        }

        final Instant end = Instant.now();
        final Duration callTime = Duration.between(start, end).minus(delay.multipliedBy(totalRequests));

        return new Metric(0, calcTimeMillis, processTimeMillis, callTime.toMillis());
    }

    private @NotNull SortResponse readResponse() throws IOException {
        readBuffer.clear();
        final int responseSize = (new DataInputStream(socket.getInputStream())).readInt();
        for (int currReadSize = 0; currReadSize < responseSize; ) {
            final int tmp = socket.getInputStream().read(readBuffer.array(), currReadSize, readBuffer.limit() - currReadSize);
            if (tmp < 0) throw new IOException("Socket closed");
            currReadSize += tmp;
        }
        readBuffer.position(responseSize);
        readBuffer.flip();
        return SortResponse.parseFrom(readBuffer);
    }


    @Override
    public void close() throws IOException {
        socket.close();
    }
}
