package org.itmo.zachyot.client;

import org.itmo.zachyot.SortRequest;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RequestRepo {

    private static final long SEED = 123456789;

    private final List<Integer> array;
    private final List<Integer> sorted;
    private final SortRequest request;

    public RequestRepo(int size) {
        final Random random = new Random(SEED);
        array = new ArrayList<>(size);
        for (int i = 0; i < size; ++i) {
            array.add(random.nextInt());
        }

        sorted = array.stream().sorted().toList();
        request = SortRequest.newBuilder().setSize(size).addAllData(array).build();
    }

    public @NotNull SortRequest getRequest() {
        return request;
    }

    public boolean checkSorted(@NotNull List<Integer> list) {
        return sorted.equals(list);
    }

}
