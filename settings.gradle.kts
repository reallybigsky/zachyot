buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("com.google.protobuf:protobuf-gradle-plugin:0.8.17")
    }
}


rootProject.name = "zachyot"

include("client")
include("server")
include("common")
include("application")
