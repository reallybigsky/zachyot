plugins {
    java
    application
    id("com.google.protobuf") apply false
}

group = "org.itmo.zachyot"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

subprojects {

    apply {
        plugin("java")
    }

    repositories {
        mavenCentral()
    }

    dependencies {
        implementation("com.google.protobuf:protobuf-java:3.19.6")
        testImplementation(platform("org.junit:junit-bom:5.9.1"))
        testImplementation("org.junit.jupiter:junit-jupiter")
    }

    sourceSets {
        main {
            java.setSrcDirs(listOf("src"))
            resources.setSrcDirs(listOf("resources"))
        }
        test {
            java.setSrcDirs(listOf("test"))
            resources.setSrcDirs(listOf("testResources"))
        }
    }

    tasks.test {
        useJUnitPlatform()
    }

    java.toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}
