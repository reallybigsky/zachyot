package org.itmo.zachyot.server.non_blocking;

import org.itmo.zachyot.SortRequest;
import org.itmo.zachyot.server.Client;
import org.itmo.zachyot.server.SortTask;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.time.Instant;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

public class NonBlockingClient extends Client implements Closeable {

    private final SocketChannel channel;
    private final Writer writer;
    private final ExecutorService handler;
    private Integer currReadMsgSize = null;
    private final Queue<ByteBuffer> writeBuffers = new ConcurrentLinkedQueue<>();

    private class Task extends SortTask {

        public Task(@NotNull SortRequest request, @NotNull Instant startHandle) {
            super(request, startHandle);
        }

        @Override
        public void run() {
            final ByteBuffer buf = ByteBuffer.wrap(getResponse().toByteArray());
            writeBuffers.add(buf);
            writer.addClient(NonBlockingClient.this);
            writer.wakeup();
        }
    }

    public NonBlockingClient(@NotNull SocketChannel channel,
                             @NotNull Writer writer,
                             @NotNull ExecutorService handler
    ) {
        this.channel = channel;
        this.writer = writer;
        this.handler = handler;
    }

    public void register(@NotNull Selector selector, int op) {
        try {
            channel.configureBlocking(false);
            channel.register(selector, op, this);
        } catch (IOException ignored) {
            close();
        }
    }

    public void readMessage() throws IOException {
        if (channel.read(readBuffer) < 0) {
            throw new IOException();
        }

        readBuffer.flip();
        while (readBuffer.remaining() > 0) {
            if (currReadMsgSize == null) {
                currReadMsgSize = readBuffer.getInt();
            }

            if (readBuffer.remaining() < currReadMsgSize) {
                break;
            }

            SortRequest request = SortRequest.parseFrom(readBuffer);
            readBuffer.position(readBuffer.position() + currReadMsgSize);
            final Instant startHandle = Instant.now();
            handler.submit(new Task(request, startHandle));
            currReadMsgSize = null;
        }
        readBuffer.compact();
    }

    public void writeMessage() throws IOException {
        while (!writeBuffers.isEmpty()) {
            ByteBuffer buf = writeBuffers.poll();
            if (buf == null) break;
            ByteBuffer size = ByteBuffer.allocateDirect(Integer.BYTES).putInt(buf.capacity()).flip();
            channel.write(size);
            while (buf.hasRemaining()) {
                channel.write(buf);
            }
        }
    }

    @Override
    public void close() {
        try {
            channel.close();
        } catch (IOException ignored) {
        }
    }

}
