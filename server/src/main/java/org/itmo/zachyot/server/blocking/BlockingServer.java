package org.itmo.zachyot.server.blocking;

import org.itmo.zachyot.server.Client;
import org.itmo.zachyot.server.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class BlockingServer extends Server {

    private final ServerSocket socket;
    private final ExecutorService clientThreadPool = Executors.newCachedThreadPool();
    private final Set<BlockingClient> clients = new HashSet<>();

    public BlockingServer(int port) throws IOException {
        this.socket = new ServerSocket(port, BACKLOG_SIZE);
    }

    @Override
    public void run() {
        while (isWorking.get()) {
            try {
                Socket clientSocket = socket.accept();
                BlockingClient client = new BlockingClient(clientSocket, isWorking, handler, clients);
                clientThreadPool.submit(client);
            } catch (IOException ignored) {
                close();
            }
        }
    }

    @Override
    public void close() {
        isWorking.set(false);
        clientThreadPool.shutdownNow();

        // Ужас? - да. Могу ли по-другому? - да. Хочу? - нет
        var it = clients.iterator();
        while (it.hasNext()) {
            BlockingClient cl = it.next();
            it.remove();
            if (cl != null) cl.close();
        }

        handler.shutdownNow();
        // Ох, я такую тему обожаю
        try {
            socket.close();
        } catch (IOException ignored) {
        }
    }

}
