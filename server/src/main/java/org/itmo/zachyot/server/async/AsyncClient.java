package org.itmo.zachyot.server.async;

import org.itmo.zachyot.SortRequest;
import org.itmo.zachyot.server.Client;
import org.itmo.zachyot.server.SortTask;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.time.Instant;
import java.util.concurrent.ExecutorService;

public class AsyncClient extends Client implements Closeable {

    private final AsynchronousSocketChannel channel;
    private final ExecutorService handler;
    private Integer currReadMsgSize = null;

    public AsyncClient(AsynchronousSocketChannel channel, ExecutorService handler) {
        this.channel = channel;
        this.handler = handler;
    }

    private class Task extends SortTask {

        public Task(@NotNull SortRequest request, @NotNull Instant startHandle) {
            super(request, startHandle);
        }

        @Override
        public void run() {
            final ByteBuffer buf = ByteBuffer.wrap(getResponse().toByteArray());
            ByteBuffer size = ByteBuffer.allocateDirect(Integer.BYTES).putInt(buf.capacity()).flip();
            channel.write(size, null, new WriteCallback(buf));
        }
    }

    private class ReadCallback implements CompletionHandler<Integer, Object> {

        @Override
        public void completed(Integer result, @Nullable Object obj) {
            readBuffer.flip();
            while (readBuffer.remaining() > 0) {
                if (currReadMsgSize == null) {
                    currReadMsgSize = readBuffer.getInt();
                }

                if (readBuffer.remaining() < currReadMsgSize) {
                    break;
                }

                try {
                    SortRequest request = SortRequest.parseFrom(readBuffer);
                    readBuffer.position(readBuffer.position() + currReadMsgSize);
                    Instant startHandle = Instant.now();
                    handler.submit(new Task(request, startHandle));
                    currReadMsgSize = null;
                } catch (IOException ignored) {
                    // Ого, он что-то делает в catch блоке, неожиданно
                    close();
                    return;
                }
            }
            readBuffer.compact();
            if (channel.isOpen()) {
                channel.read(readBuffer, null, this);
            }
        }

        @Override
        public void failed(Throwable exc, @Nullable Object obj) {
            close();
        }
    }

    private class WriteCallback implements CompletionHandler<Integer, Object> {

        private final ByteBuffer writeBuffer;

        public WriteCallback(@NotNull ByteBuffer buf) {
            this.writeBuffer = buf;
        }

        @Override
        public void completed(Integer result, @Nullable Object attachment) {
            if (channel.isOpen() && writeBuffer.hasRemaining()) {
                channel.write(writeBuffer, null, this);
            }
        }

        @Override
        public void failed(Throwable exc, @Nullable Object attachment) {
            close();
        }
    }

    public void startAsync() {
        if (channel.isOpen()) {
            channel.read(readBuffer, null, new ReadCallback());
        }
    }

    @Override
    public void close() {
        // К этому можно привыкнуть...
        try {
            channel.close();
        } catch (Exception ignored) {
        }
    }

}
