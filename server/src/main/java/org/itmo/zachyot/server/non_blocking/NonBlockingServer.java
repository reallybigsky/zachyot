package org.itmo.zachyot.server.non_blocking;

import org.itmo.zachyot.server.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class NonBlockingServer extends Server {

    private final ServerSocketChannel socketChannel = ServerSocketChannel.open();
    private final Reader reader = new Reader(isWorking);
    private final Writer writer = new Writer(isWorking);

    public NonBlockingServer(int port) throws IOException {
        this.socketChannel.bind(new InetSocketAddress(port), BACKLOG_SIZE);
    }

    @Override
    public void run() {
        Thread readerThread = new Thread(reader);
        Thread writerThread = new Thread(writer);

        readerThread.setDaemon(true);
        writerThread.setDaemon(true);

        readerThread.start();
        writerThread.start();

        while (isWorking.get()) {
            try {
                SocketChannel clientChannel = socketChannel.accept();
                NonBlockingClient client = new NonBlockingClient(clientChannel, writer, handler);
                reader.addClient(client);
                reader.wakeup();
            } catch (IOException ignored) {
                close();
            }
        }
    }

    @Override
    public void close() {
        isWorking.set(false);

        reader.close();
        writer.close();

        handler.shutdownNow();
        try {
            socketChannel.close();
        } catch (IOException ignored) {
        }
    }

}
