package org.itmo.zachyot.server.async;

import org.itmo.zachyot.server.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Executors;


public class AsyncServer extends Server {
    private final AsynchronousChannelGroup acg = AsynchronousChannelGroup.withThreadPool(Executors.newSingleThreadExecutor());
    private final AsynchronousServerSocketChannel socketChannel = AsynchronousServerSocketChannel.open(acg);

    public AsyncServer(int port) throws IOException {
        this.socketChannel.bind(new InetSocketAddress(port), BACKLOG_SIZE);
    }

    @Override
    public void run() {
        while (isWorking.get()) {
            try {
                AsynchronousSocketChannel clientChannel = socketChannel.accept().get();
                AsyncClient client = new AsyncClient(clientChannel, handler);
                client.startAsync();
            } catch (Exception ignored) {
                close();
            }
        }
    }

    @Override
    public void close() {
        isWorking.set(false);
        handler.shutdownNow();
        try {
            acg.shutdownNow();
            socketChannel.close();
        } catch (IOException ignored) {
            // Знакомая тема
        }
    }

}
