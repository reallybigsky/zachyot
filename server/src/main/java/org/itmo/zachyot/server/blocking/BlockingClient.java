package org.itmo.zachyot.server.blocking;

import org.itmo.zachyot.SortRequest;
import org.itmo.zachyot.SortResponse;
import org.itmo.zachyot.server.Client;
import org.itmo.zachyot.server.SortTask;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.time.Instant;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class BlockingClient extends Client implements Runnable, Closeable {

    private final ExecutorService writeExecutor = Executors.newSingleThreadExecutor();
    private final Socket socket;
    private final AtomicBoolean isWorking;
    private final ExecutorService handler;
    private final Set<BlockingClient> clientSet;

    public BlockingClient(@NotNull Socket socket,
                          @NotNull AtomicBoolean isWorking,
                          @NotNull ExecutorService handler,
                          @NotNull Set<BlockingClient> clients
    ) {
        this.socket = socket;
        this.isWorking = isWorking;
        this.handler = handler;
        this.clientSet = clients;
        this.clientSet.add(this);
    }

    @Override
    public void run() {
        try {
            while (isWorking.get()) {
                SortRequest request = readRequest();
                final Instant startHandle = Instant.now();
                Task task = new Task(request, startHandle);
                handler.submit(task);
            }
        } catch (IOException ignored) {
            // Бывает же такое
        }

        close();
    }

    private @NotNull SortRequest readRequest() throws IOException {
        readBuffer.clear();
        final int requestSize = (new DataInputStream(socket.getInputStream())).readInt();
        for (int currReadSize = 0; currReadSize < requestSize; ) {
            final int tmp = socket.getInputStream().read(readBuffer.array(), currReadSize, readBuffer.limit() - currReadSize);
            if (tmp < 0) throw new IOException("Socket closed"); // Вау, мы даже сами их бросаем
            currReadSize += tmp;
        }
        readBuffer.position(requestSize);
        readBuffer.flip();
        return SortRequest.parseFrom(readBuffer);
    }

    private class Task extends SortTask {

        public Task(@NotNull SortRequest request, @NotNull Instant startHandle) {
            super(request, startHandle);
        }

        @Override
        public void run() {
            final SortResponse response = getResponse();

            writeExecutor.submit(() -> {
                try {
                    final byte[] bytes = response.toByteArray();
                    new DataOutputStream(socket.getOutputStream()).writeInt(bytes.length);
                    socket.getOutputStream().write(bytes);
                    socket.getOutputStream().flush();
                } catch (IOException ignored) {
                    close();
                }
            });
        }
    }

    @Override
    public void close() {
        clientSet.remove(this);
        writeExecutor.shutdown();
        try {
            socket.close();
        } catch (IOException ignored) {
        }
    }

}
