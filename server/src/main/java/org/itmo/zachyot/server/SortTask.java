package org.itmo.zachyot.server;

import org.itmo.zachyot.SortRequest;
import org.itmo.zachyot.SortResponse;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public abstract class SortTask implements Runnable {

    protected final SortRequest request;
    protected final Instant startHandle;

    public SortTask(@NotNull SortRequest request, @NotNull Instant startHandle) {
        this.request = request;
        this.startHandle = startHandle;
    }

    public SortResponse getResponse() {
        final List<Integer> result = new ArrayList<>(request.getDataList());
        final Instant startCalc = Instant.now();
        Utils.heavyComputation(result);
        final Instant end = Instant.now();

        return SortResponse.newBuilder()
                .setSize(result.size())
                .setCalcTimeMillis(Duration.between(startCalc, end).toMillis())
                .setProcessTimeMillis(Duration.between(startHandle, end).toMillis())
                .addAllData(result)
                .build();
    }

}
