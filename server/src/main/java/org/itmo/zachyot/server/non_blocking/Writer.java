package org.itmo.zachyot.server.non_blocking;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Writer implements Runnable, Closeable {

    private final AtomicBoolean isWorking;
    private final Selector selector = Selector.open();
    private final Queue<NonBlockingClient> registerQueue = new ConcurrentLinkedQueue<>();

    public Writer(AtomicBoolean isWorking) throws IOException {
        this.isWorking = isWorking;
    }

    public void addClient(NonBlockingClient client) {
        registerQueue.add(client);
    }

    public void wakeup() {
        selector.wakeup();
    }

    @Override
    public void run() {
        try {
            while (isWorking.get()) {
                selector.select();
                if (!isWorking.get()) break;
                var it = selector.selectedKeys().iterator();
                while (it.hasNext()) {
                    var selectedKey = it.next();
                    if (selectedKey.isWritable()) {
                        NonBlockingClient client = (NonBlockingClient) selectedKey.attachment();
                        try {
                            client.writeMessage();
                        } catch (IOException ignored) {
                            client.close();
                        }
                        selectedKey.cancel();
                    }
                    it.remove();
                }

                while (!registerQueue.isEmpty()) {
                    NonBlockingClient client = registerQueue.poll();
                    if (client == null) break;
                    client.register(selector, SelectionKey.OP_WRITE);
                }
            }
        } catch (IOException ignored) {
            // Добрый день
        }
        close();
    }

    @Override
    public void close() {
        try {
            selector.close();
        } catch (IOException ignored) {
        }
    }
}
