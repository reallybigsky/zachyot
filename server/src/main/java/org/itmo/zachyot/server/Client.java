package org.itmo.zachyot.server;

import java.nio.ByteBuffer;

public abstract class Client {

    protected static final int READ_BUFFER_SIZE = 100 * 1024;
    protected final ByteBuffer readBuffer = ByteBuffer.allocate(READ_BUFFER_SIZE);

}
