package org.itmo.zachyot.server;

import java.io.Closeable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Server implements Runnable, Closeable {

    protected static final int BACKLOG_SIZE = 1024;
    protected static final int WORKING_THREADS_COUNT = Math.max(1, Runtime.getRuntime().availableProcessors() / 2);
    protected final ExecutorService handler = Executors.newFixedThreadPool(WORKING_THREADS_COUNT);
    protected final AtomicBoolean isWorking = new AtomicBoolean(true);

}
