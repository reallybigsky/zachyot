package org.itmo.zachyot.server.non_blocking;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Reader implements Runnable, Closeable {

    private final AtomicBoolean isWorking;
    private final Selector selector = Selector.open();
    private final Queue<NonBlockingClient> registerQueue = new ConcurrentLinkedQueue<>();

    public Reader(AtomicBoolean isWorking) throws IOException {
        this.isWorking = isWorking;
    }

    public void addClient(NonBlockingClient client) {
        registerQueue.add(client);
    }

    public void wakeup() {
        selector.wakeup();
    }

    @Override
    public void run() {
        try {
            while (isWorking.get()) {
                selector.select();
                if (!isWorking.get()) break;
                var it = selector.selectedKeys().iterator();
                while (it.hasNext()) {
                    var selectedKey = it.next();
                    if (selectedKey.isReadable()) {
                        NonBlockingClient client = (NonBlockingClient) selectedKey.attachment();
                        try {
                            client.readMessage();
                        } catch (IOException ignored) {
                            // Ого, оно даже обрабатывается
                            client.close();
                            selectedKey.cancel();
                        }
                    }
                    it.remove();
                }

                while (!registerQueue.isEmpty()) {
                    NonBlockingClient client = registerQueue.poll();
                    if (client == null) break;
                    client.register(selector, SelectionKey.OP_READ);
                }
            }
        } catch (IOException ignored) {
            // Ну да
        }
        close();
    }

    @Override
    public void close() {
        try {
            selector.close();
        } catch (IOException ignored) {
        }
    }
}
