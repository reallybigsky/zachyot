package org.itmo.zachyot.server;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class Utils {

    // Сортировка выбором (O(n^2) при любом входе) in-place
    public static <T extends Comparable<? super T>>
    void heavyComputation(@NotNull List<T> input) {
        for (int i = 0; i < input.size(); ++i) {
            int pos = i;
            for (int j = i + 1; j < input.size(); ++j) {
                if (input.get(j).compareTo(input.get(pos)) < 0) {
                    pos = j;
                }
            }
            T tmp = input.get(pos);
            input.set(pos, input.get(i));
            input.set(i, tmp);
        }
    }

}
